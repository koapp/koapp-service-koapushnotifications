(function () {
  angular
    .module('king.services.koapushnotifications', [])
    .run(loadFunction);

  loadFunction.$inject = ['configService', '$http', '$rootScope'];

  function loadFunction(configService, $http, $rootScope) {
    // Register upper level modules
    try {
      if (configService.services && configService.services.koapushnotifications) {
        notificationFunction($http, configService.services.koapushnotifications.scope, $rootScope);
      } else {
        throw "The service is not added to the application";
      }
    } catch (error) {
      console.error("Error", error);
    }
  }

  function notificationFunction($http, scopeData, $rootScope) {
    $rootScope.push ={
      accessToken: scopeData.accessToken,
      pushServer: scopeData.pushServer
    };
    //check when device ready
    document.addEventListener("deviceready", onDeviceReady, false);
    function onDeviceReady() {

      FCM.hasPermission().then((permission)=>{
        if(!permission){
          FCM.requestPushPermission({
            ios9Support: {
              timeout: 10,  // How long it will wait for a decision from the user before returning `false`
              interval: 0.3 // How long between each permission verification
            }
          }).then((event)=>{ 
            console.log(event); 
            getToken();
          }).catch((e)=>{error(e)});
        }else{
          getToken();
        }
        
        //get device token
        function getToken(){
          FCM.getToken().then(function (token) {
            if (!localStorage.getItem('deviceToken')) {
              localStorage.setItem('deviceToken', token);
            }
            $rootScope.push.deviceToken = token;
            connectToKoaPush(token);
            console.log("deviceToken:", token);
          }).catch((e)=>{error(e)});
        }
      }).catch((e)=>{error(e)});

      //send device token to Smart IO Labs
      function connectToKoaPush(deviceToken) {
        console.log("connectToKoaPush");
        var payload = new FormData();
        payload.append("Access_Token", scopeData.accessToken);

        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;

        xhr.open("POST", `${scopeData.pushServer}/api/save_device/?device_token=${deviceToken}&device_type=android&channels_id=1,2`);
        xhr.setRequestHeader("access_token", deviceToken);

        xhr.send(payload);
      }

      //throw notifications
      FCM.onNotification(function (notification) {
        console.log(notification);
        var notificationFix = notification.body? notification:
                              notification.notification && notification.notification.body? 
                              notification.notification: null;
        if(notificationFix){
          if (notificationFix.title) {
            navigator.notification.alert(notificationFix.body, function () { }, notificationFix.title);
          } else {
            navigator.notification.alert("", function () { }, notificationFix.body);
          }
        }else{
          console.error("ERROR notification structure")
        }
      });
    }

    function error(error){
      console.log(error);
    }
  }
  // --- End servicenameController content ---
})();