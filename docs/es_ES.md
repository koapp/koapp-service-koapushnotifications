# Documentación

### koa push notifications configuración:

Para tener notificaciones push en tu aplicación sigue estos pasos:

 - Abre tu cuenta de firebase o [crea una aquí](https://firebase.google.com/) si aún no la tienes.
 - Crea un proyecto o accede a uno creado previamente.
 - Agrega tu app al proyecto usando el id del paquete proporcionado por el builder. (Pendiente de ampliar ésta parte)
 - En el menú de la izquierda selecciona la opción configuración del proyecto 

![configuración del proyecto](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/koapushnotifications/firebase_config1.png)

- En el menú a la derecha selecciona la opción **Cloud Messaging** 

![Cloud Messaging](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/koapushnotifications/firebase_config2.png)

- En la sección **Credenciales del proyecto** copia el contenido del campo clave del servidor heredada

![Credenciales del proyecto](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/koapushnotifications/firebase_config3.png)

- De regreso en el builder dirígete al a la sección **Push** del menú de la izquierda.
- El sistema te guiará para registrar tu app y vincularla a nuestro sistema. (Pendiente de ampliar ésta parte)
- En el apartado **Api key** pega el número recogido anteriormente de firebase.

![Api key](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/koapushnotifications/firebase_config4.png)

- Al hacer click en **Save changes** aparecerá una vista como ésta:

![Save changes](https://s3-us-west-2.amazonaws.com/images.docs.kingofapp.com/koapushnotifications/firebase_config5.png)

- Copia el contenido del campo **Access Token** y pégalo en el campo correspondiente en la configuración de servicio de notificaciones push.
- Con un editor de código abre los archivos obtenidos de firebase (al crear las agregar las aplicaciones para android e iOS) copia su contenido en los campos de la configuración correspondiente a cada archivo señalado con el nombre del archivo)
- ¡Has clic en el botón de guardar y listo!
